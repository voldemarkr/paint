package com.example.paint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class DrawClass extends View {

    private Bitmap image;
    private Canvas canvas;

    public DrawClass(Context context){
        super(context);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom){
        super.onLayout(changed, left, top, right, bottom);
        Bitmap.Config config = Bitmap.Config.RGB_565;
        image = Bitmap.createBitmap(getWidth(), getHeight(), config);
        canvas = new Canvas(image);
        canvas.drawColor(Color.WHITE);
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);

        Paint myPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        myPaint.setColor(Color.BLACK);

        canvas.drawBitmap(image, 0,0, myPaint);
    }

    public Bitmap getImage() {
        return image;
    }
    public Canvas getCanvas(){
        return canvas;
    }
}
