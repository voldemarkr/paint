package com.example.paint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.SeekBar;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private DrawClass myDraw;
    private Canvas canvas;
    private Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
    private SeekBar seekBar;
    private SettingsConfig settingsSave;

    private int mode = MODE_PENCIL;

    final static int MODE_PENCIL = 0;
    final static int MODE_LINE = 1;

    float x1, y1, a, b, c, r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDraw = new DrawClass(this);
        myDraw.setId(View.generateViewId());
        seekBar = findViewById(R.id.seekBar);
        settingsSave = new SettingsConfig(this);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                p.setStrokeWidth(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        FrameLayout layout = findViewById(R.id.frameLayout);
        layout.addView(myDraw);

        p.setStrokeWidth(10);
        p.setColor(Color.BLACK);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        canvas = myDraw.getCanvas();

        int[] location = new int[2];
        myDraw.getLocationOnScreen(location);
        switch (mode) {
            case MODE_PENCIL:
                canvas.drawCircle(event.getX(), event.getY() - location[1], p.getStrokeWidth(), p);
            break;
            case MODE_LINE:
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    x1 = event.getX();
                    y1 = event.getY();
                } else if(event.getAction() == MotionEvent.ACTION_UP){
                    canvas.drawLine(x1, y1 - location[1], event.getX(), event.getY() - location[1], p);
                }
                break;
            default:
                return true;
        }
        myDraw.invalidate();

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pencil_mode:
                mode = MODE_PENCIL;
                break;
            case R.id.line_mode:
                mode = MODE_LINE;
                break;
            case R.id.eraser_mode:
                mode = MODE_PENCIL;
                p.setColor(Color.WHITE);
                break;
            default:
                p.setColor(v.getBackgroundTintList().getDefaultColor());
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public void toSettings() {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    public void onClick(MenuItem item) throws IOException {
        switch (item.getItemId()) {
            case R.id.clear:
                myDraw.getCanvas().drawColor(Color.WHITE);
                myDraw.invalidate();
                break;
            case R.id.settings:
                toSettings();
                break;
            case R.id.saving:
                SaverImages saverImages = new SaverImages(settingsSave.getImageFormat(), settingsSave.getImageQuality(), settingsSave.getStorePath());
                saverImages.save(myDraw.getImage());
                break;
            default:
                break;
        }
    }
}