package com.example.paint;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SaverImages {
    private static String path;
    private static int qualityImage;
    private static Bitmap.CompressFormat formatFile;

    public SaverImages(Bitmap.CompressFormat formatFile, int qualityImage, String path) {
        this.formatFile = formatFile;
        this.qualityImage = qualityImage;
        this.path = path;
    }

    public static void save(Bitmap image) throws IOException {
        File file = new File(genFName());
        file.createNewFile();
        FileOutputStream out = new FileOutputStream(file);
        image.compress(formatFile, qualityImage, out);
        out.flush();
        out.close();
    }

    public static String genFName(){
        String filename = "", type;
        if(formatFile == Bitmap.CompressFormat.JPEG){
            type = "jpg";
        } else { type = "png"; }
        filename = path + '/' + new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss").format(new Date()) + '.' + type;
        return filename;
    }

    public String getPath() {
        return path;
    }

    public Bitmap.CompressFormat getFormat() {
        return formatFile;
    }

    public int getQuality() {
        return qualityImage;
    }
}
