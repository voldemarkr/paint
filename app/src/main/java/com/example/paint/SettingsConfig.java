package com.example.paint;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Environment;

import androidx.preference.PreferenceManager;

public class SettingsConfig {
    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    public SettingsConfig(Context context){
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
    }




    String getStorePath() {
        String storePath = preferences.getString(context.getString(R.string.path), null);
        if (storePath == "" || storePath == null) {
            storePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/";

            editor.putString(context.getString(R.string.path), storePath);

            editor.commit();

        }
        return storePath;
    }

    int getImageQuality() {
        int quality = preferences.getInt(context.getString(R.string.quality), -1);

        if (quality == -1) {
            quality = 95;

            editor.putInt(context.getString(R.string.quality), quality);
            editor.commit();
        }
        return quality;
    }

    Bitmap.CompressFormat getImageFormat() {
        String format = preferences.getString(context.getString(R.string.format), null);
        if (format == null) {
            format = "png";
            editor.putString(context.getString(R.string.format), format);
            editor.commit();
        }
        switch (format) {
            case "png":
                return Bitmap.CompressFormat.PNG;
            case "jpeg":
                return Bitmap.CompressFormat.JPEG;
            default:
                return null;
        }
    }
}
